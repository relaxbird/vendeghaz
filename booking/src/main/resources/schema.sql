CREATE TABLE IF NOT EXISTS booking (
  id VARCHAR(36) PRIMARY KEY,
  arrivaldate DATE NOT NULL,
  leavingdate DATE NOT NULL,
  firstname VARCHAR(64) NOT NULL,
  lastname VARCHAR(64) NOT NULL,
  reservationstatus BOOLEAN DEFAULT TRUE
);
