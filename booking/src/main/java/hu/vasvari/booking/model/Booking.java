package hu.vasvari.booking.model;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

public class Booking {

	private String id;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate arrivalDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate leavingDate;
	
	private String firstName;
	private String lastName;
	private boolean reservationStatus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(LocalDate arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public LocalDate getLeavingDate() {
		return leavingDate;
	}

	public void setLeavingDate(LocalDate leavingDate) {
		this.leavingDate = leavingDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(boolean reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	@Override
	public String toString() {
		return "Booking [id=" + id + ", arrivalDate=" + arrivalDate + ", leavingDate=" + leavingDate + ", firstName="
				+ firstName + ", lastName=" + lastName + ", reservationStatus=" + reservationStatus + "]";
	}

}
