package hu.vasvari.booking.repository;

import static org.springframework.util.Assert.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import hu.vasvari.booking.model.Booking;

@Repository
public class BookingRepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private BookingRowMapper brm;

	public List<Booking> get() {
		return jdbcTemplate.query("SELECT * FROM booking", new Object[] {}, brm);
	}

	public List<Booking> getByReservationStatus(boolean reservationStatus) {
		return jdbcTemplate.query("SELECT * FROM booking WHERE reservationstatus=?", new Object[] { reservationStatus }, brm);
	}

	public void add(Booking b) {
		notNull(b, "The entity can not be null");
		jdbcTemplate.update("INSERT INTO booking (id, arrivaldate, leavingdate, firstname, lastname, reservationstatus) VALUES (?, ?, ?, ?, ?, ?)",
				b.getId(), b.getArrivalDate(), b.getLeavingDate(), b.getFirstName(), b.getLastName(), b.isReservationStatus());
	}

	public void edit(Booking b) {
		notNull(b, "The entity can not be null");
		jdbcTemplate.update("UPDATE booking set arrivaldate=?, leavingdate=?, firstname=?, lastname=?, reservationstatus=? where id = ?", 
				b.getArrivalDate(), b.getLeavingDate(), b.getFirstName(), b.getLastName(), b.isReservationStatus(), b.getId());
	}

	public void remove(String id) {
		notNull(id, "The entity can not be null");
		jdbcTemplate.update("DELETE FROM booking where id = ?", id);
	}
	
	public Booking queryById(String id) {
		notNull(id, "The entity can not be null");
		Booking b = jdbcTemplate.queryForObject("SELECT * FROM booking where id = ?", new Object[] { id }, brm);
		if(b == null) throw new EmptyResultDataAccessException(1);
		return b;
	}

}
