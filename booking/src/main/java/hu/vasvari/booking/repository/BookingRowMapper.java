package hu.vasvari.booking.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import hu.vasvari.booking.model.Booking;

@Component
public class BookingRowMapper implements RowMapper<Booking> {

	@Override
	public Booking mapRow(ResultSet rs, int rowNum) throws SQLException {
		Booking b = new Booking();

		b.setId(rs.getString("id"));
		b.setArrivalDate(rs.getDate("arrivaldate").toLocalDate());
		b.setLeavingDate(rs.getDate("leavingdate").toLocalDate());
		b.setFirstName(rs.getString("firstname"));
		b.setLastName(rs.getString("lastname"));
		b.setReservationStatus(rs.getBoolean("reservationstatus"));

		return b;
	}

}
