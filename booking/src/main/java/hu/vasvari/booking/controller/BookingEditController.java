package hu.vasvari.booking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import hu.vasvari.booking.model.Booking;
import hu.vasvari.booking.service.BookingService;

@Controller
public class BookingEditController {

	@Autowired
	BookingService service;

	@GetMapping("/foglalas/")
	public ModelAndView createNewBooking() {
		ModelAndView mav = new ModelAndView("booking");
		Booking b = new Booking();
		mav.addObject("booking", b);
		mav.addObject("create", true);
		return mav;
	}

	@GetMapping("/foglalas/{id}/")
	public ModelAndView showBookingData(@PathVariable String id) {
		ModelAndView mav = new ModelAndView("booking");
		Booking b = service.get(id);
		mav.addObject("booking", b);
		mav.addObject("create", false);
		return mav;
	}

	@PostMapping("/foglalas/")
	public String bookingCreateOrEdit(Booking b) {		
		if (b.getId() == null || b.getId().isEmpty()) {
			service.create(b);
		} else {
			service.edit(b);
		}
		return "redirect:/foglalasok/";
	}

	@ExceptionHandler
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String bookingNotFound(EmptyResultDataAccessException e) {
		return "booking-not-found";
	}
}
