package hu.vasvari.booking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import hu.vasvari.booking.model.Booking;
import hu.vasvari.booking.service.BookingService;

@Controller
public class BookingListController {
	
	@Autowired
	BookingService service;

	@GetMapping("/foglalasok/")
	public ModelAndView activeBookingList() {
		List<Booking> bookings = service.currentReservations();
		ModelAndView mav = new ModelAndView("booking-list");
		mav.addObject("bookingList", bookings);
		return mav;
	}

	@PostMapping("/foglalasok/{id}/lezar")
	public String bookingClose(@PathVariable String id) {
		service.bookingClose(id);
		return "redirect:/foglalasok/";
	}

	@ExceptionHandler
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String bookingNotFound(EmptyResultDataAccessException e) {
		return "booking-not-found";
	}
}
