package hu.vasvari.booking.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hu.vasvari.booking.model.Booking;
import hu.vasvari.booking.repository.BookingRepository;

@Service
public class BookingService {

	@Autowired
	BookingRepository repository;

	public List<Booking> pastReservations() {
		return repository.getByReservationStatus(false);
	}

	public List<Booking> currentReservations() {
		return repository.getByReservationStatus(true);
	}

	public Booking get(String id) {
		Booking b = repository.queryById(id);
		return b;
	}

	public void create(Booking b) {
		b.setId(UUID.randomUUID().toString());
		b.setReservationStatus(true);
		repository.add(b);
	}

	public void edit(Booking b) {
		b.setReservationStatus(true);
		repository.edit(b);
	}

	public void remove(String id) {
		repository.remove(id);
	}

	@Transactional
	public void bookingClose(String id) {
		Booking b = repository.queryById(id);
		b.setReservationStatus(false);
		repository.edit(b);
	}

}
